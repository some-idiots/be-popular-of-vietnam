import config from "~/config";
import { Op } from "sequelize";
import { Topic, TopicInterface } from "~/models/Topic";
import { GridInterface } from "@models/transformers/Grid";
import { TopicItemInterface } from "~/models/TopicItem";
import { DEFAULT_CATEGORIES, DEFAULT_STATUS } from "@util/constants";
import connection from "~/database";
import { QueryTypes } from "sequelize";

export const getTopicByCategory = async (cate: string, page: number): Promise<GridInterface<TopicInterface>> => {
    const limit: number = parseInt(config.DEFAULT_PAGE_SIZE as string);
    const offset: number = page <= 1 ? 0 : page - 1;
    const result = await Topic.findAndCountAll({
        attributes: ["id", "author_id", "title", "slug", "image"],
        limit: limit,
        offset: offset,
        where: {
            category: {[Op.in]: cate ? [cate] : DEFAULT_CATEGORIES},
            status: DEFAULT_STATUS.ACTIVE,
        }
    });

    const transformerData: GridInterface<TopicInterface> = {
        data: result.rows,
        total: result.count,
        page: page,
        pageSize: limit
    };

   return transformerData;
};

export const getTopicByHash = async (hash: string): Promise<TopicInterface> => {
    const result: TopicInterface = await Topic.findOne({
        where: {
            hash: hash,
        }
    });
   return result;
};

export const getTopicItemByTopic = async (identify: string, page: number, search: string): Promise<GridInterface<TopicItemInterface>> => {
    const limit: number = parseInt(config.DEFAULT_PAGE_SIZE as string);
    const result: TopicItemInterface[] = await connection.query(
        `
            select topic_items.*, count(*) OVER() AS count_all 
            from topic_items
            inner join topic_item_refs on topic_items.id = topic_item_refs.item_id
            inner join topics on topics.id = topic_item_refs.topic_id and topics.hash = :hash
            where topic_items.title like :search
            limit :limit
            offset :offset
        `
        , {
            replacements: { 
                hash: identify,
                search: `%${search}%`,
                limit: limit,
                offset: limit*page,
            }, 
            type: QueryTypes.SELECT 
        });
        
    const transformerData: GridInterface<TopicItemInterface> = {
        data: result,
        total: parseInt(result[0].count_all),
        page: page+1,
        pageSize: limit
    };
    
    return transformerData;
};
