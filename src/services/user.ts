import { User } from "~/entity/User";
import { createQueryBuilder, getRepository } from "typeorm";

const userRepository = getRepository<User>(User);

export const getUserLogin = async () => {
    await userRepository
    .createQueryBuilder("user")
    .where("user.id = :id", { id: 1 })
    .getOne();
};