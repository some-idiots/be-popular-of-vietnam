import express from "express";

const router = express.Router();

import topicController from "./controllers/topic";
import userController from "./controllers/user";

router.use("/topic", topicController);

router.use("/user", userController);

export default router;