// import {Request, Response, NextFunction} from "express";
// import { match } from "node-match-path";
// import { getAllApplicationRoutes } from "@services/application";
// import * as ModelApiPermission from "@models/ApiPermission";
// import {ApiRouteInterface} from "@models/transformers/ApiRoute";

// export const accessControl = async (req: Request, res: Response, next: NextFunction) => {

//     // Get user data after auth middleware
//     const user: any = req.user;
//     const fullPath: string = req.baseUrl + req.path;
//     const listApis: ApiRouteInterface[] = getAllApplicationRoutes();

//     // Get regex path from request
//     const pathRegex = listApis.find( (api: ApiRouteInterface) => match(api.path, fullPath).matches);
//     if (!pathRegex) res.status(404).send({error: "Not found resource"});

//     // Check permission
//     const permission: ModelApiPermission.ApiPermissionInterface = await ModelApiPermission.ApiPermission.findOne({
//         where: {
//             RoleID: user.RoleID,
//             Url: pathRegex.path,
//             Method: req.method
//         }
//     });
//     if (permission) {
//         next();
//     } else {
//         res.status(403).send({ error: "Not permission to access this resource" });
//     };
// };