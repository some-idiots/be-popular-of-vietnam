// import jwt from "jsonwebtoken";
// import * as ModelAdminProfile from "../models/AdminProfile";

// export const auth = async(req: any, res: any, next: any) => {
//     const token = req.header("Authorization").replace("Bearer ", "");
//     const data: any = jwt.verify(token, process.env.JWT_KEY, (err: any, decoded: any) => {
//         if(err!=null){
//             return res.sendStatus(401);
//         }
//         return decoded;
//     });
//     try {
//         const user = await ModelAdminProfile.AdminProfile.findOne({ where: { AccountID: data._id, JwtToken: token } });
//         if (user==null) {
//             return res.sendStatus(403);
//         }
//         req.user = user;
//         req.token = token;
//         next();
//     } catch (error) {
//         res.status(401).send({ error: "Not authorized to access this resource" });
//     }

// };