"use strict";

import { Model, DataTypes } from "sequelize";
import db from "DatabaseConnection";

// User.hasMany(models.post);

export interface UserInterface extends Model {
    id: number;
    uuid: string;
    username: string;
    name: string;
    email: number;
    password: string;
    admin: number;
    blocked: number;
    archived: number;
    last_login: Date;
    createdAt: Date;
    updatedAt: Date;
}

export const User = db.define<UserInterface>("User", {
    id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
    },
    uuid: {
        allowNull: false,
        type: "user_id"
    },
    username: {
        type: DataTypes.STRING
    },
    name: {
        type: DataTypes.STRING
    },
    email: {
        type: DataTypes.STRING
    },
    password: {
        type: DataTypes.STRING
    },
    admin: {
        defaultValue: false,
        type: DataTypes.BOOLEAN
    },
    blocked: {
        defaultValue: false,
        type: DataTypes.BOOLEAN
    },
    archived: {
        defaultValue: false,
        type: DataTypes.BOOLEAN
    },
    last_login: {
        allowNull: true,
        type: DataTypes.DATE
    },
    createdAt: {
        allowNull: false,
        type: DataTypes.DATE
    },
    updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
    }
}, {
    tableName: "users",
    updatedAt: true,
    createdAt: true
});