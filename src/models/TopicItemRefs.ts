"use strict";

import { Model, DataTypes } from "sequelize";
import db from "DatabaseConnection";

export interface TopicItemRefsInterface extends Model {
    item_id: number;
    topic_id: number;
    author_id: number;
    createdAt: Date;
    updatedAt: Date;
}

export const TopicItemRefs = db.define<TopicItemRefsInterface>("TopicItem", {
    item_id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.INTEGER
    },
    topic_id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.INTEGER
    },
    author_id: {
        allowNull: false,
        type: DataTypes.INTEGER
    },
    createdAt: {
        allowNull: false,
        type: DataTypes.DATE
    },
    updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
    }
}, {
    tableName: "topic_item_refs",
    updatedAt: true,
    createdAt: true
});