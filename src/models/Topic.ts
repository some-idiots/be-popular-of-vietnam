"use strict";

import { Model, DataTypes } from "sequelize";
import db from "DatabaseConnection";
import { TopicItem, TopicItemInterface } from "./TopicItem";
import { TopicItemRefs } from "./TopicItemRefs";

export interface TopicInterface extends Model {
    id: number;
    author_id: number;
    category: string;
    title: string;
    slug: string;
    question: string;
    content: string;
    image: string;
    createdAt: Date;
    updatedAt: Date;
    TopicItems: TopicItemInterface[];
};

export const Topic = db.define<TopicInterface>("Topic", {
    id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
    },
    author_id: {
        type: DataTypes.INTEGER
    },
    category: {
        type: DataTypes.STRING
    },
    title: {
        type: DataTypes.STRING
    },
    slug: {
        type: DataTypes.STRING
    },
    question: {
        type: DataTypes.STRING
    },
    content: {
        type: DataTypes.STRING
    },
    image: {
        type: DataTypes.STRING
    },
    createdAt: {
        allowNull: false,
        type: DataTypes.DATE
    },
    updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
    }
}, {
    tableName: "topics",
    updatedAt: true,
    createdAt: true
});

// Topic.belongsTo(User, {
//     as: "topic_of_user"
// });

Topic.belongsToMany(TopicItem, { through: TopicItemRefs, foreignKey: "topic_id", otherKey: "item_id" });
TopicItem.belongsToMany(Topic, { through: TopicItemRefs, foreignKey: "item_id", otherKey: "topic_id"});