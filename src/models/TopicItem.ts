"use strict";

import { Model, DataTypes } from "sequelize";
import db from "DatabaseConnection";

export interface TopicItemInterface extends Model {
    id: number;
    author_id: number;
    category: string;
    title: string;
    slug: string;
    content: string;
    image: string;
    status: number;
    createdAt: Date;
    updatedAt: Date;
    count_all: string;
};

export const TopicItem = db.define<TopicItemInterface>("TopicItem", {
    id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
    },
    author_id: {
        type: DataTypes.INTEGER
    },
    category: {
        type: DataTypes.STRING
    },
    title: {
        type: DataTypes.STRING
    },
    slug: {
        type: DataTypes.STRING
    },
    content: {
        type: DataTypes.STRING
    },
    image: {
        type: DataTypes.STRING
    },
    status: {
        type: DataTypes.INTEGER
    },
    createdAt: {
        allowNull: false,
        type: DataTypes.DATE
    },
    updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
    }
}, {
    tableName: "topic_items",
    updatedAt: true,
    createdAt: true
});

