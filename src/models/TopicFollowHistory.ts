"use strict";

import { User } from "./User";
import { Topic } from "./Topic";
import db from "DatabaseConnection";
import { Model, DataTypes } from "sequelize";

export interface TopicFollowHistoryInterface extends Model {
    id: number;
    author_id: number;
    topic_id: number;
    createdAt: Date;
    updatedAt: Date;
};

export const TopicFollowHistory = db.define<TopicFollowHistoryInterface>("TopicFollowHistory", {
    id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
    },
    author_id: {
        type: DataTypes.INTEGER
    },
    topic_id: {
        type: DataTypes.INTEGER
    },
    createdAt: {
        allowNull: false,
        type: DataTypes.DATE
    },
    updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
    }
}, {
    tableName: "topic_follow_history",
    updatedAt: true,
    createdAt: true
});

TopicFollowHistory.belongsTo(User);
TopicFollowHistory.belongsTo(Topic);