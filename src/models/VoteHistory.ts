"use strict";

import { Model, DataTypes } from "sequelize";
import db from "DatabaseConnection";
import { User } from "./User";
import { TopicItem } from "./TopicItem";

export interface VoteHistoryInterface extends Model {
    id: number;
    author_id: number;
    topic_item_id: number;
    topic_id: number;
    createdAt: Date;
    updatedAt: Date;
};

export const VoteHistory = db.define<VoteHistoryInterface>("VoteHistory", {
    id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
    },
    author_id: {
        type: DataTypes.INTEGER
    },
    topic_item_id: {
        type: DataTypes.INTEGER
    },
    topic_id: {
        type: DataTypes.INTEGER
    },
    point: {
        type: DataTypes.INTEGER
    },
    createdAt: {
        allowNull: false,
        type: DataTypes.DATE
    },
    updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
    }
}, {
    tableName: "topics",
    updatedAt: true,
    createdAt: true
});

VoteHistory.belongsTo(User);
VoteHistory.belongsTo(TopicItem);