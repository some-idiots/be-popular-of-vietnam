import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class CreateUserTable1608458219351 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const rawData = await queryRunner.query("CREATE DOMAIN user_id AS TEXT CHECK(VALUE ~ '^[0-9a-z]{11}$')");

        await queryRunner.createTable(new Table({
            name: "users",
            columns: [
                {
                    name: "id",
                    isNullable: false,
                    isGenerated: true,
                    generationStrategy: "increment",
                    isPrimary: true,
                    type: "int"
                },
                {
                    name: "uuid",
                    isNullable: false,
                    type: "user_id"
                },
                {
                    name: "username",
                    type: "varchar"
                },
                {
                    name: "name",
                    type: "varchar"
                },
                {
                    name: "email",
                    type: "varchar"
                },
                {
                    name: "password",
                    type: "varchar"
                },
                {
                    name: "admin",
                    default: false,
                    type: "boolean"
                },
                {
                    name: "blocked",
                    default: false,
                    type: "boolean"
                },
                {
                    name: "archived",
                    default: false,
                    type: "boolean"
                },
                {
                    name: "last_login",
                    isNullable: true,
                    type: "date"
                },
                {
                    name: "createdAt",
                    isNullable: false,
                    type: "date"
                },
                {
                    name: "updatedAt",
                    isNullable: false,
                    type: "date"
                }
            ]
        }), true);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        const table = await queryRunner.getTable("users");
        await queryRunner.dropTable("users");
        await queryRunner.query("DROP DOMAIN IF EXISTS user_id");
    }

}
