'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("vote_history", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      author_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "users",
          key: "id"
        },
      },
      topic_item_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "topic_items",
          key: "id"
        },
      },
      topic_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "topics",
          key: "id"
        },
      },
      point: {
        type: Sequelize.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("vote_history");
  }
};
