"use strict";
const fs = require("fs");
const nanoid = require("nanoid");
const prettier = require("prettier");
const { name, date, image, internet, random } = require("faker");

const alphabet = "0123456789abcdefghijklmnopqrstuvwxyz";
const newUserId = nanoid.customAlphabet(alphabet, 11);

const jsonFile = `${__dirname}/01_users.json`;

function stringify(obj) {
  return prettier.format(JSON.stringify(obj), { parser: "json" });
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let users = fs.existsSync(jsonFile) ? require(jsonFile) : null;
    if (!users) {
      console.log("Generating users.json...");
      const usernames = new Set();
  
      users = Array.from({ length: 200 }).map((elem, index) => {
        const uuid = newUserId();
        const gender = random.arrayElement(["male", "female"]);
        const firstName = name.firstName(gender);
        const lastName = name.lastName(gender);
        let username = `${firstName.toLowerCase()}${random.number(50)}`;
        const createdAt = date.recent(365);

        // Ensures that the username is unique
        while (usernames.has(username)) {
          username = `${firstName.toLowerCase()}${random.number(50)}`;
        }
        usernames.add(username);
        return {
          id: index+1,
          uuid,
          username,
          name: `${firstName} ${lastName}`,
          email: internet.email(firstName, lastName).toLowerCase(),
          password: "cc3a0280e4fc1415930899896574e118",
          admin: false,
          blocked: false,
          archived: false,
          createdAt: createdAt,
          updatedAt: createdAt,
          last_login:
            date.between(createdAt, new Date()),
        };
      });
      fs.writeFileSync(jsonFile, stringify(users), "utf8");
    }
    users.forEach((x) => {
      x.uuid = x.uuid.substring(0, 11);
    });
    fs.writeFileSync(jsonFile, stringify(users), "utf8");
    return await queryInterface.bulkInsert("users", users, {});
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("users", null, {});
  }
};
