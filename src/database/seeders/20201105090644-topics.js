"use strict";

const fs = require("fs");
const prettier = require("prettier");
const slugify = require("slugify");
const { date, image, lorem, internet } = require("faker");

const defaultCategories = [
  "movie",
  "book",
  "food",
  "travel",
  "other"
];

const jsonFile = `${__dirname}/02_topics.json`;
const jsonUsersFile = `${__dirname}/01_users.json`;
function stringify(obj) {
  return prettier.format(JSON.stringify(obj), { parser: "json" });
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let topics = fs.existsSync(jsonFile) ? require(jsonFile) : null;
    const users = fs.existsSync(jsonUsersFile) ? require(jsonUsersFile) : null;

    if (!topics) {
      console.log("Generating topics.json...");
      const slugs = new Set();
  
      topics = Array.from({ length: 100 }).map((currentElem, index, arr) => {
        const category = defaultCategories[Math.floor(Math.random() * defaultCategories.length)];
        const user = users[Math.floor(Math.random() * users.length)];
        const author_id = user.id;
  
        const title = lorem.sentence();
        let slug = slugify(title);
        const hash = internet.password(12, false, /[0-9A-Za-z]/);
        const question = lorem.words(8);
        const content = lorem.sentences(4);
        const imageUrl = image.imageUrl(320,640);
        const createdAt = date.recent(365);
  
        // Ensures that the slug is unique
        while (slugs.has(slug)) {
          slug = `${slugify(title)}-${random.number(1000)+index}`;
        }
  
        slugs.add(slug);
  
        return {
          id: index,
          category,
          author_id,
          title,
          slug,
          hash,
          question,
          content,
          image: imageUrl,
          status: "active",
          createdAt: createdAt,
          updatedAt: createdAt,
        };
      });
  
      fs.writeFileSync(jsonFile, stringify(topics), "utf8");
    }
  
    fs.writeFileSync(jsonFile, stringify(topics), "utf8");
    return await queryInterface.bulkInsert("topics", topics, {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("topics", null, {});
  }
};
