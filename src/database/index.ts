"use strict";

import { Sequelize } from "sequelize";
import config from "~/config";

const { DB_HOST, DB_NAME, DB_USERNAME, DB_PASSWORD } = config;

const connection = new Sequelize(DB_NAME, DB_USERNAME, DB_PASSWORD, {
    host: DB_HOST,
    dialect: "postgres",
    dialectOptions: {
        options: {
          requestTimeout: 60000
        }
    },
});

export default connection;
