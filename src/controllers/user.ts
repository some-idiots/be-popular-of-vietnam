import express from "express";
import { Request, Response } from "express";
import { getUserLogin } from "~/services/user";

const router = express.Router();

router.get("/login", async (req: Request, res: Response) => {
    try {
        const user = await getUserLogin();
        res.json(user);
    } catch (e) {
        res.status(400).send({error: e.message});
    }
});

export default router;