import express from "express";
import { Request, Response } from "express";
import { TopicInterface } from "~/models/Topic";
import { DEFAULT_CATEGORIES } from "@util/constants";
import { getTopicByCategory, getTopicByHash, getTopicItemByTopic } from "@services/topic";
import { GridInterface } from "@models/transformers/Grid";
import { TopicItemInterface } from "~/models/TopicItem";
import { TopicFollowHistory } from "~/models/TopicFollowHistory";

const router = express.Router();

router.get("/topicByCategory", async (req: Request, res: Response) => {
    try {
        const cate: string = req.query.cate as string || "";
        if(!DEFAULT_CATEGORIES.includes(cate)){
            res.status(400);
        }
        const page: number = parseInt(req.query.page as string) || 1;
        const result: GridInterface<TopicInterface> = await getTopicByCategory(cate, page);
        res.json(result);
    } catch (e) {
        res.status(400).send({error: e.message});
    }
});

router.get("/show", async (req: Request, res: Response) => {
    const slug: string = req.query.slug as string || "";
    const search: string = req.query.search as string || "";
    const uuid: string = req.query.uuid as string || "";

    const hash: string = slug.split("-").pop();
    const page: number = (parseInt(req.query.page as string) || 1) - 1;

    const topic = await getTopicByHash(hash);
    // $list = UserMarkHistories::query()
    //         ->select(['mark_type', 'item_id'])
    //         ->where('user_id', $userId)
    //         ->where('challenge_id', $this->id)
    //         ->get();
    // $marks = $challenge->getMarks(Auth::user()->id);
    // $isFollowed = UserFollowTopic::query()
    //     ->where('challenge_id', $challenge->id)
    //     ->where('user_id', Auth::user()->id)
    //     ->exists();
    // const isFollowed: boolean = await TopicFollowHistory.findOne({
    //     where: [
    //         {topic_id: topic.id},
    //         {author_id: uuid},
    //     ]
    // });
    const listItem: GridInterface<TopicItemInterface> = await getTopicItemByTopic(hash, page, search);
    
    // $listCheckedFromDB = UserCheckedHistories::query()
    //     ->where('user_id', Auth::user()->id)
    //     ->where('challenge_id', $challenge->id)
    //     ->get()->pluck('item_id')->toArray();

    // return view('challenge.detail', [
    //     'searchKeyword' => $searchKeyword,
    //     'currentPage' => $page,
    //     'totalPage' => $totalPage,
    //     'totalItem' => $totalItem,
    //     'challenge' => $challenge,
    //     'marks' => $marks,
    //     'listItemPage' => $listItemPage,
    //     'isFollowed' => $isFollowed,
    //     'listCheckedFromDB' => $listCheckedFromDB
    // ]);
    
    res.json({
        topic,
        listItem,
        search
    });
});

export default router;