import {createConnections} from "typeorm";
const connections = async () => { 
    await createConnections([{
        name: "meta",
        type: "postgres",
        host: "localhost",
        port: 5432,
        username: "challenge",
        password: "secret",
        database: "challenge_meta",
        entities: [__dirname + "/entity/*{.js,.ts}"],
        synchronize: true
    }, {
        name: "db2Connection",
        type: "mysql",
        host: "localhost",
        port: 3306,
        username: "root",
        password: "admin",
        database: "db2",
        entities: [__dirname + "/entity/*{.js,.ts}"],
        synchronize: true
    }]);
};

export default connections;