export const DEFAULT_CATEGORIES = [
    "movie",
    "book",
    "food",
    "travel",
    "other"
];

export const CHALLENGE_ITEM_MARKS = [
    "upvoted",
    "downvoted",
    "todo",
    "favorited"
];

export const DEFAULT_STATUS = {
    ACTIVE: "active",
    DEACTIVE: "deactive",
};