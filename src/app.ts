import cors from "cors";
import morgan from "morgan";
import express from "express";
import config from "./config";
import apiRouter from "./api-routes";
import bodyParser from "body-parser";
import session from "express-session";
// import authRouter from "./auth-routes";
// import { auth } from "./middleware/adminauth";
// import { accessControl } from "./middleware/access-control";
import compression from "compression";
import "reflect-metadata";

const app = express();

app.use(compression());
app.use(morgan("dev")); // sử dụng để log mọi request ra console
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({
    resave: true,
    saveUninitialized: true,
    secret: config.SESSION_SECRET,
}));

app.use(cors());
app.options("*", cors());
// app.use("/", authRouter);
// app.use("/api", auth, accessControl, apiRouter);
app.use("/api", apiRouter);

export default app;