import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity({database: "meta"})
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column("string",{nullable:true})
    uuid: string;

    @Column("string",{nullable:true})
    username: string;

    @Column("string",{nullable:true})
    name: string;

    @Column("number",{nullable:true})
    email: number;

    @Column("string",{nullable:true})
    password: string;

    @Column("number",{nullable:true})
    admin: number;

    @Column("number",{nullable:true})
    blocked: number;

    @Column("number",{nullable:true})
    archived: number;

    @Column("date",{nullable:true})
    last_login: Date;

    @Column("date",{nullable:true})
    createdAt: Date;

    @Column("date",{nullable:true})
    updatedAt: Date;
}
