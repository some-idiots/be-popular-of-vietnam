require("dotenv/config");

module.exports = {
   name: "meta",
   type: "postgres",
   host: process.env.TYPEORM_HOST,
   port: process.env.TYPEORM_PORT,
   username: process.env.TYPEORM_USERNAME,
   password: process.env.TYPEORM_PASSWORD,
   database: process.env.TYPEORM_DATABASE,
   entities: ["dist/**/*.entity{.ts,.js}"],
   synchronize: true,
   migrationsTableName: "migration",
   migrations: ["src/typeorm/migration/*.ts"],
   cli: {
      migrationsDir: "migration",
      entitiesDir: "src/entity",
      migrationsDir: "src/typeorm/migration",
      subscribersDir: "src/typeorm/subscriber"
   },
};